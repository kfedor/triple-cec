#include <iostream>
#include "cec.h"

int main()
{
	try
	{
		TripleCEC tricec( "AI043NFV" );
		
		tricec.getStatus();
		tricec.turnOnAll();
	}

	catch( std::exception &ex )
	{
		std::cout << "Fial: " << ex.what() << std::endl;
	}
}
