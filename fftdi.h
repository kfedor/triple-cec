#include <ftdi.h>
#include <vector>
#include <stdexcept>
#include <string>
#include <iostream>

class FTDI
{
	public:
		FTDI( std::string );
		~FTDI();

		int sendRecv( std::vector<unsigned char> );
		int sendAckNak( std::vector<unsigned char> );
		int readBytes();
		std::vector<unsigned char> reply_vector;

	private:
		struct ftdi_context ftdic;

	protected:
		unsigned int protoLen;
		unsigned int protoLenPos;
};
