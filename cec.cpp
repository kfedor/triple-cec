#include "cec.h"
/*
[11:51:52 AM] [COX] LostSoul: Федь если вдруг неймецца можешь попосылать это -
SndCMD -> FD:03:10:00:ED:FE
SndCMD -> FD:03:10:01:EC:FE
SndCMD -> FD:03:10:02:EB:FE
SndCMD -> FD:02:03:FB:FE
[11:52:12 AM] [COX] LostSoul: первые 3 это включение телека попорядку. последняя получение ста
*/

void printHex( std::string some, std::vector<unsigned char> data )
{
	unsigned int sz = data.size();

	printf( "%s [%u]: ", some.c_str(), sz );

	for( unsigned int i=0; i<sz; i++ )
		printf( "%02X ", data[i] );

	printf( "\n" );
}

int TripleCEC::getStatus()
{
	std::vector<unsigned char> cmd;

	cmd.push_back( sysdata::STX );
	cmd.push_back( 2 );    // len
	cmd.push_back( 0x03 ); // cmd
	cmd.push_back( 0xFB ); // crc
	cmd.push_back( sysdata::ETX );

	printHex( "getStatus: ", cmd );

	if( sendAckNak( cmd ) )
	{
		if( readBytes() )
		{
			printHex( "getStatus OK: ", reply_vector );
			return 1;
		}
	}

	printHex( "getStatus fail: ", reply_vector );

	return 0;
}

int TripleCEC::turnOnAll()
{
	std::vector<unsigned char> cmd;

	// TV 0
	cmd.push_back( sysdata::STX );
	cmd.push_back( 3 );    // len
	cmd.push_back( 0x10 ); // cmd
	cmd.push_back( 0x00 ); // tv 0
	cmd.push_back( 0xED ); // crc
	cmd.push_back( sysdata::ETX );

	printHex( "turnOn 0: ", cmd );

	if( !sendAckNak( cmd ) )
		printf( "turnOn 0 Fail\n" );
	else
		printf( "turnOn 0 OK\n" );

	// TV 1
	cmd[3] = 0x01;
	cmd[4] = 0xEC;

	printHex( "turnOn 1: ", cmd );

	if( !sendAckNak( cmd ) )
		printf( "turnOn 1 Fail\n" );
	else
		printf( "turnOn 1 OK\n" );
		
	// TV 2
	cmd[3] = 0x02;
	cmd[4] = 0xEB;

	printHex( "turnOn 2: ", cmd );

	if( !sendAckNak( cmd ) )
		printf( "turnOn 2 Fail\n" );
	else
		printf( "turnOn 2 OK\n" );

	return 1;
}
