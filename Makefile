CEFLIBS = -L./bin
CC = c++ -std=c++11 -O2 -Wall -Wl,--no-as-needed -pedantic -Wnon-virtual-dtor -Wunreachable-code -Winit-self -Woverloaded-virtual
FILES = $(wildcard *.cpp)
LIBS = -lpthread -lftdi
OUT = -o bin/cec

all:
	$(CC) $(INC) $(OUT) $(FILES) $(GTKLIBS) $(CEFLIBS) $(LIBS)
