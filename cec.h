#ifndef _CEC
#define _CEC

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include "fftdi.h"

class TripleCEC : public FTDI
{
	public:
		enum sysdata
		{
		    ACK = 0xFA,
		    NAK = 0xFB,
		    STX = 0xFD,
		    ETX = 0xFE
		};

		TripleCEC( std::string serial ) : FTDI( serial )
		{
			protoLen = 3;
			protoLenPos = 1;
		}
		int getStatus();
		int turnOnAll();

	private:

};

#endif
