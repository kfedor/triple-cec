#include "fftdi.h"

FTDI::FTDI( std::string req_serial )
{
	if( ftdi_init( &ftdic ) < 0 )
		throw std::runtime_error( "ftdi_init failed" );

	ftdi_set_interface( &ftdic, INTERFACE_A );

	printf( "Searching for FTDI dev serial [%s]\n", req_serial.c_str() );

	if( ftdi_usb_open_desc( &ftdic, 0x0403, 0x6001, NULL, req_serial.c_str() ) < 0 )
		throw std::runtime_error( "unable to open ftdi device\n" );

	ftdi_set_baudrate( &ftdic, 9600 );
	reply_vector.reserve( 255 );

	protoLen = 0;
	protoLenPos = 0;

	//unsigned int chipid;
	//printf( "ftdi_read_chipid: %d\n", ftdi_read_chipid( &ftdic, &chipid ) );
	//printf( "FTDI chipid: %X\n", chipid );
}


FTDI::~FTDI()
{
	ftdi_usb_close( &ftdic );
	ftdi_deinit( &ftdic );
}

int FTDI::sendRecv( std::vector<unsigned char> cmd )
{
	unsigned char tmp = 0;

	if( ftdi_write_data( &ftdic, cmd.data(), cmd.size() ) < 0 )
		return 0;

	reply_vector.clear();

	int estimated_length = -1;

	for( int i=0; i<100; i++ )
	{
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			reply_vector.push_back( tmp );


			if( estimated_length < 0 && reply_vector.size() > protoLenPos )
			{
				estimated_length = reply_vector[protoLenPos];
				printf( "Estimated LENGTH [%02X]\n", estimated_length );
			}

			if( reply_vector.size() == estimated_length+protoLen )
				return 1;
		}
		else
		{
			usleep( 1000 );
		}
	}

	return 0;
}

int FTDI::sendAckNak( std::vector<unsigned char> cmd )
{
	unsigned char tmp = 0;

	if( ftdi_write_data( &ftdic, cmd.data(), cmd.size() ) < 0 )
		return 0;

	for( int i=0; i<100; i++ )
	{
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			switch( tmp )
			{
				case 0xFB:
					printf( "NAK\n" );
					return 0;

				case 0xFA:
					printf( "ACK\n" );
					return 1;
			}
		}
		else
		{
			usleep( 500 );
		}
	}

	printf( "Timeout\n" );
	return 0;
}

int FTDI::readBytes()
{
	unsigned char tmp = 0;

	reply_vector.clear();

	int estimated_length = -1;

	for( int i=0; i<100; i++ )
	{
		int bread = ftdi_read_data( &ftdic, ( unsigned char* )&tmp, 1 );

		if( bread > 0 )
		{
			reply_vector.push_back( tmp );

			if( estimated_length < 0 && reply_vector.size() > protoLenPos )
			{
				estimated_length = reply_vector[protoLenPos];
				printf( "Estimated LENGTH [%02X]\n", estimated_length );
			}

			if( reply_vector.size() == estimated_length+protoLen )
				return 1;
		}
		else
		{
			usleep( 1000 );
		}
	}

	return 0;
}
